import org.junit.Test;
import redis.clients.jedis.Jedis;

/**
 * Created by Wangwang on 2017/7/30.
 */
public class TestConnect {
    @Test
    public void testTwo() {
        Jedis jedis = new Jedis("127.0.0.1");
        jedis.auth("123");
        System.out.println("连接成功");
        // 查看服务是否运行
        System.out.println("running status : " + jedis.ping());
    }

}
