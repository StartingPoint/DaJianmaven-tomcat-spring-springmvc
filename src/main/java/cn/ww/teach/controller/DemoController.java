package cn.ww.teach.controller;

import cn.ww.teach.service.IDemoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Wangwang on 2017/7/30.
 */
@Controller
@RequestMapping("/ctrl")
@Slf4j
public class DemoController {
    @Autowired
    IDemoService demoService;

    @RequestMapping("/demo/test")
    @ResponseBody
    public String testDemoCtrl() {
        return demoService.testDemo();
    }

    @RequestMapping("/demo/redisSet")
    @ResponseBody
    public String testRdisSet(){
        if(demoService.testRedisSet()){
            return "success";
        }
        return "fail";
    }
    @RequestMapping("/demo/redisGet")
    @ResponseBody
    public String testRedisGet(){
            return demoService.testRedisGet();
    }

}
