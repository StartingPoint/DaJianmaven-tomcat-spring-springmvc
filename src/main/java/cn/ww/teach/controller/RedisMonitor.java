package cn.ww.teach.controller;

import cn.ww.teach.service.IRedisMonitorService;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Set;

/**
 * Created by wangwang on 2017/7/31 0031.
 */
@Controller
@RequestMapping("/monitor")
public class RedisMonitor {
    @Autowired
    IRedisMonitorService redisMonitorService;

    @RequestMapping("/redis/getKeys")
    @ResponseBody
    public String getRedisKeys() {
        return JSON.toJSONString(redisMonitorService.queryRedisKeys());
    }

    @RequestMapping("/redis/getValues")
    @ResponseBody
    public String getRedisValue(String key) {
        return redisMonitorService.queryRedisValues(key);
    }
}
