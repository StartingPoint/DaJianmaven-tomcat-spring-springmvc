package cn.ww.teach.service;

import java.util.Set;

/**
 * Created by wangwang on 2017/7/31 0031.
 */
public interface IRedisMonitorService {
    Set<String> queryRedisKeys();

    String queryRedisValues(String key);
}
