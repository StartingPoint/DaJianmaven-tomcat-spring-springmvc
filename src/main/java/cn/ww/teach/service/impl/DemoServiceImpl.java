package cn.ww.teach.service.impl;

import cn.ww.teach.service.IDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * Created by Wangwang on 2017/7/30.
 */
@Service
public class DemoServiceImpl implements IDemoService {
    @Autowired
    private JedisPool pool;

    @Override
    public String testDemo() {

        return "ok";
    }

    @Override
    public boolean testRedisSet() {
        Jedis resource = pool.getResource();
        resource.set("testKey", "testValue");
        return true;
    }

    @Override
    public String testRedisGet() {
        Jedis resource = pool.getResource();
        Object testValue = resource.get("testKey");
        return String.valueOf(testValue);
    }
}
