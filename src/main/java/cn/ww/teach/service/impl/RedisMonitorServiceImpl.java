package cn.ww.teach.service.impl;

import cn.ww.teach.service.IRedisMonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Set;

/**
 * Created by wangwang on 2017/7/31 0031.
 */
@Service
public class RedisMonitorServiceImpl implements IRedisMonitorService {
    @Autowired
    private JedisPool pool;
    @Override
    public  Set<String> queryRedisKeys() {
        Jedis resource = pool.getResource();
        Set<String> keys = resource.keys("*");
        return keys;
    }

    @Override
    public String queryRedisValues(String key) {
        Jedis resource = pool.getResource();
        return  resource.get(key);
    }
}
