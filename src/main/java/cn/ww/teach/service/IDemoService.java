package cn.ww.teach.service;

/**
 * Created by Wangwang on 2017/7/30.
 */
public interface IDemoService {
    String testDemo();

    boolean testRedisSet();

    String testRedisGet();
}
